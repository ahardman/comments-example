<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function showComments()
    {
        return view('comments', [
            'comments' => Comment::with('user')->latest()->get(),
        ]);
    }

    public function storeComment(Request $request)
    {
        $request->validate([
            'body' => 'required|string',
        ]);
        $request->user()->comments()->create([
            'body' => $request->input('body'),
        ]);
        return redirect()->route('dashboard');
    }
}
