<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Comments') }}
        </h2>
    </x-slot>

    <div class="max-w-2xl mx-auto py-6 p-4 sm:p-6 lg:p-8 my-6 bg-white border shadow rounded-md">
        <form action="{{ route('dashboard') }}" method="post" class="pb-6 mb-6 border-b">
            @csrf

            @if ($errors->any())
                <div class="mb-3">
                    <div class="font-medium text-red-600">
                        {{ __('Whoops! Something went wrong.') }}
                    </div>

                    <ul class="mt-3 list-disc list-inside text-sm text-red-600">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div>
                <x-label for="body" :value="__('Comment')" />
                <textarea name="body" class="w-full rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" required></textarea>
            </div>

            <x-button>
                {{ __('Submit') }}
            </x-button>
        </form>

        @forelse ($comments as $comment)
            <div class="mt-4">
                <div class="flex mb-1">
                    <div class="text-indigo-500 font-semibold mr-2">
                        {{ $comment->user->name }}
                    </div>
                    <div class="text-gray-600">
                        {{ $comment->created_at->format('F j, Y g:ia') }}
                    </div>
                </div>
                <div class="border-l-2 pl-2">
                    {{ $comment->body }}
                </div>
            </div>
        @empty
            <div class="py-6 text-center">
                No comments yet.
            </div>
        @endforelse
    </div>
</x-app-layout>
